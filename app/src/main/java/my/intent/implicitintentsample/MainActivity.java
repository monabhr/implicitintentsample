package my.intent.implicitintentsample;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button open_website_button, location_button, share_button;
    EditText share_text, location_text, website_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //received intent
       /* Intent intent = getIntent();
        Uri uri = intent.getData();
        if (uri != null) {
            String uri_string = getString(R.string.label)
                    + uri.toString();
            TextView textView = findViewById(R.id.text_message);
            textView.setText(uri_string);
        }*/
       
        bindViews();


    }

    private void bindViews() {
        open_website_button = findViewById(R.id.open_website_button);
        location_button = findViewById(R.id.location_button);
        share_button = findViewById(R.id.share_button);

        share_text = findViewById(R.id.share_text);
        location_text = findViewById(R.id.location_text);
        website_text = findViewById(R.id.website_text);


        open_website_button.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.open_website_button:
                openWebPage();

                break;

            case R.id.share_button:
                openLocation();
                break;

            case R.id.location_button:
                share();
                break;

        }


    }

    private void share() {

        String shareTxt = share_text.getText().toString();

        String mimeType = "text/plain";

        ShareCompat.IntentBuilder
                .from(this)
                .setType(mimeType)
                .setChooserTitle("Share this text with: ")
                .setText(shareTxt)
                .startChooser();
    }


    private void openWebPage() {

        String url = website_text.getText().toString();

        // Parse the URI and create the intent.
        Uri webAdress = Uri.parse(url);

        Intent intent = new Intent(Intent.ACTION_VIEW, webAdress);

        //Use the resolveActivity() method and the Android package manager to find an Activity
        // that can handle your implicit Intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d("ImplicitIntents", "Can't handle this!");
        }
    }

    private void openLocation() {

        String location = location_text.getText().toString();

        // Parse the location and create the intent.
        Uri address = Uri.parse("geo:0,0?q=" + location);
        Intent intent = new Intent(Intent.ACTION_VIEW, address);

        // Find an activity to handle the intent, and start that activity
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d("ImplicitIntents", "Can't handle this intent!");
        }
    }


}
